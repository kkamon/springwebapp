package com.springBlog.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.springBlog.entity.Blog;
import com.springBlog.entity.Item;

//second parameter must be a primary key of the entity
public interface ItemRepository extends JpaRepository<Item, Integer>{
	
	List<Item> findByBlog(Blog blog, Pageable pageble);
}
