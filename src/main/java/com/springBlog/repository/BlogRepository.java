package com.springBlog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.springBlog.entity.Blog;
import com.springBlog.entity.User;


//second parameter must be a primary key of the entity
public interface BlogRepository extends JpaRepository<Blog, Integer>{
	List<Blog> findByUser(User user);

}
