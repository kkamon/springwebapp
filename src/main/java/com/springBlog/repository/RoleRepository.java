package com.springBlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springBlog.entity.Role;

//second parameter must be a primary key of the entity
public interface RoleRepository extends JpaRepository<Role, Integer>{

	Role findByName(String name);

}
