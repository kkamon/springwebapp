package com.springBlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springBlog.entity.User;

//second parameter must be a primary key of the entity
public interface UserRepository extends JpaRepository<User, Integer>{

	User findByName(String name);

}
